import psycopg2

try:
    conn = psycopg2.connect(database = "youtube1", user = "postgres", password = "admin", host = "localhost", port = "5432")
except:
    print("I am unable to connect to the database")

cur = conn.cursor()
try:
    cur.execute("CREATE TABLE video (videoID VARCHAR(255) PRIMARY KEY, uploader VARCHAR(255) NOT NULL,age INT,"
                "category VARCHAR(255),length DECIMAL,views INT,rate DECIMAL,ratings INT,comments INT,relatedID TEXT [])")
    print("TABLE CREATED")
except:
    print("I can't drop our test database!")

conn.commit() # <--- makes sure the change is shown in the database
conn.close()