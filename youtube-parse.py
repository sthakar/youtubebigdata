myFile = open("0222/0.txt", "r")
data = str(myFile.read())
myFile = open("0222/1.txt", "r")
data += str(myFile.read())
myFile = open("0222/2.txt", "r")
data += str(myFile.read())
myFile = open("0222/3.txt", "r")
data += str(myFile.read())
myFile = open("0222/4.txt", "r")
data += str(myFile.read())
data = data.replace("\t", ",")
tokenList = data.split("\n")
formedData = []
for tokenString in tokenList:
    tempDict = {
        "videoID":"",
        "uploader":"",
        "age":0,
        "category":"",
        "length":0,
        "views":0,
        "rate":0.0,
        "ratings":0,
        "comments":0,
        "relatedID":[]
    }
    tokens = tokenString.split(",")
    count = 0
    for token in tokens:
        if(count is 0):
            tempDict["videoID"] = token
        if(count is 1):
            tempDict["uploader"] = token
        if(count is 2):
            tempDict["age"] = int(token)
        if(count is 3):
            tempDict["category"] = token
        if(count is 4):
            tempDict["length"] = int(token)
        if(count is 5):
            tempDict["views"] = int(token)
        if(count is 6):
            tempDict["rate"] = float(token)
        if(count is 7):
            tempDict["ratings"] = int(token)
        if(count is 8):
            tempDict["comments"] = int(token)
        if(count > 8):
            tempDict["relatedID"].append(token)
        count += 1
        if(tempDict["videoID"] is not ""):
            formedData.append({tempDict["videoID"]:tempDict})
print len(tokenList)
print formedData[0]